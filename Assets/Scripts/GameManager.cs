using System.Collections;
using System.Collections.Generic;   
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    [SerializeField] private string levelsPrefix;
    [SerializeField] private GameObject mainCamera;

    private int currentLevel = -1;
    private bool levelLoaded = false;

#region Singleton
    public static GameManager instance;
    private List<string> levels;

	void Awake ()
	{
		instance = this;
        levels = this.findAllScenesWithPrefix(levelsPrefix);
	}
#endregion

    public void Start() {
        MenuManager.OnPlayPressed += Unpause;
        MenuManager.OnPausePressed += Pause;
        MenuManager.OnBackToMainPressed += UnloadCurrentLevel;
        MenuManager.OnExitPressed += ExitApplication;
    }

    public bool gamePaused {get; private set;}

    /**
     * Scene Management
     */
    private List<string> findAllScenesWithPrefix(string prefix) {
        List<string> scenes = new List<string>();
        string sceneName;

        for (int index = 1; index < SceneManager.sceneCountInBuildSettings; index ++) {
            sceneName = prefix + index.ToString();
            Scene scene = SceneManager.GetSceneByName(sceneName);
            if (scene != null) {
                scenes.Add(sceneName);
            }
        }

        return scenes;
    }

    private void LoadLevel(int level) {
        if (currentLevel != -1) {
            UnloadCurrentLevel();
        }
        // load next
        currentLevel = level;
        
        string levelName = getLevelNameByIndex(level);
        Debug.Log(levelName);
        if (levelName != null) {
            SceneManager.LoadScene(levelName, LoadSceneMode.Additive);
            levelLoaded = true;
            mainCamera.SetActive(false);
        }
        else
            Debug.Log("Thanks for playing");
            // TODO  Load Final
        
    }

    private void UnloadCurrentLevel() {
        if (SceneManager.GetSceneByName(getLevelNameByIndex(currentLevel)) != null && 
            SceneManager.GetSceneByName(getLevelNameByIndex(currentLevel)).isLoaded) {
            ClearCurrentScene();
            SceneManager.UnloadScene(getLevelNameByIndex(currentLevel));
        }
    }

    private void ClearCurrentScene() {
        levelLoaded = false;
        mainCamera.SetActive(true);
    }

    private string getLevelNameByIndex(int levelIndex) {
        if (levels.Count > levelIndex - 1)
            return levels[levelIndex - 1];

        return null;
    }

    public void LoadNextScene() {
        if (currentLevel != -1) {
            LoadLevel(currentLevel + 1);
        } else {
            LoadLevel(0);
        }
    }

    /**
     * Pause and Unpause
     */
    public void Pause() {
        gamePaused = true;
        Debug.Log("Game Paused");
        Time.timeScale = 0;
    }

    public void Unpause() {
        Time.timeScale = 1;
        if (!levelLoaded)
            LoadNextScene();
        gamePaused = false;
        Debug.Log("Game Unpaused");
    }

    public void ExitApplication() {
        Debug.Log("Exiting application");
        // Are you sure?
        // Save data if needed?
        Application.Quit();
    }
}
