using UnityEngine;

public class PausedState : IState
{
    public bool unpause;
    public bool backToMain;
    private MenuManager stateMachine;

    public PausedState(MenuManager stateMachine) {
        this.stateMachine = stateMachine;
        this.backToMain = false;
        this.stateMachine.continueButton?.onClick.AddListener(OnContinuePressed);
        this.stateMachine.backToMainButton?.onClick.AddListener(OnBackToMainPressed);
    }

    public void Tick() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Debug.Log("Escape pressed");
            unpause = true;
        }
    }

    public void OnEnter() {
        stateMachine.pauseMenuPanel.SetActive(true);
        stateMachine.PausePressed();
        unpause = false;
        backToMain = false;
    }

    public void OnExit() {
        unpause = false;
        stateMachine.pauseMenuPanel.SetActive(false);
    }

    private void OnContinuePressed() {
        unpause = true;
    }

    private void OnBackToMainPressed() {
        backToMain = true;
    } 
}