using System;
using UnityEngine;
using UnityEngine.UI;

//MenuManager.OnPlayPressed += PlayPressed;
public class MenuManager : MonoBehaviour {
    [Header("Menu Panels")]
    [SerializeField] public GameObject mainMenuPanel;
    [SerializeField] public GameObject pauseMenuPanel;
    [SerializeField] public GameObject inGameUIPanel;

    [Header("Main Menu Buttons")]
    [SerializeField] public Button play;
    [SerializeField] public Button credits;
    [SerializeField] public Button exit;

    [Header("Pause Buttons")]
    [SerializeField] public Button continueButton;
    [SerializeField] public Button backToMainButton;

    public static event Action OnPlayPressed;
    public static event Action OnExitPressed;
    public static event Action OnPausePressed;
    public static event Action OnBackToMainPressed;

    private StateMachine stateMachine;
    private InGameState inGame;
    private MainMenuState mainMenu;
    private PausedState paused;

    void Awake() {
        stateMachine = new StateMachine();

        inGame = new InGameState(this);
        mainMenu = new MainMenuState(this);
        paused = new PausedState(this);

        stateMachine.AddTransition(mainMenu, inGame, () => mainMenu.playPressed);
        stateMachine.AddTransition(inGame, paused, () => inGame.escPressed);
        stateMachine.AddTransition(paused, inGame, () => paused.unpause);
        stateMachine.AddTransition(paused, mainMenu, () => paused.backToMain);

        stateMachine.SetState(mainMenu);
    }

    void Update () {
        stateMachine.Tick();
    }

    public void backToMainPressed() {
        OnBackToMainPressed?.Invoke();
    }

    public void PlayPressed() {
        OnPlayPressed?.Invoke();
    }

    public void PausePressed() {
        OnPausePressed?.Invoke();
    }

    public void ExitPressed() {
        // TODO Are you sure?
        OnExitPressed?.Invoke();
    }
}
